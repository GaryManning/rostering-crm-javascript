Rostering Project (CRM)

Includes:

Base Roster configuration tool
Sits in an Iframe of the Base Roster main page. Reads the current base roster and displays it in a table in the Iframe. Reads the available services (activities, duties, whatever) and allows them to be dragged and dropped into the available spaces.

Swap Request message formatter
Reads the current message list from rsl_allmessages (in JSON format) and formats into an easily (human) readable format for the Swap Request form. Also adds any message entered in the rsl_messages field to the rsl_allmessages field.

Base Configuration file structure:
index.html - mis-named, is really rsl_baseConfig.html (the Iframe content)
rsl_roster/scripts/ajax.js - ajax functions
rsl_roster/scripts/baseConfig.js - main javascript file, contains most of the functions for the configuration tool
rsl_roster/styles/baseConfig.css - stylesheet for the configuration tool

Swap Request messages formatter file structure:
swapsWebResource.html - (couldn't think of a better name) - the contents of the CRM Web Resource
rsl_roster/scripts/swapsLibary.js - contains the main library functions for Swap Request message formatting