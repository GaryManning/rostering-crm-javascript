var server = "https://raspberry.mycrmhosted.net/";
var oData = "XRMServices/2011/OrganizationData.svc/";

var getServices = get_CRM_Services();
//var getServices = get_Local_Services();

function createRosterDetail(updateObj) {
	var jsonEntity = window.JSON.stringify(updateObj);

	return $.ajax({
		type: "POST",
		contentType: "application/json; charaset=utf-8",
		datatype: "json",
		data: jsonEntity,
		url: server + oData + "rsl_baserosterdetailSet",
		beforeSend: function (XMLHttpRequest) {
			//Specifiying this header ensures that the results will be returned as JSON
			XMLHttpRequest.setRequestHeader("Accept", "application/json");
			XMLHttpRequest.setRequestHeader("Content-Type", "application/json; charset=utf-8");
		},
		success: function (data, textStatus, XmlHttpRequest) {
			//alert("roster details saved.");	//TODO make this a ui dialog
		},
		error: function (XmlHttpRequest, textStatus, errorThrown) {
			if (XmlHttpRequest && XmlHttpRequest.responseText ) {
				alert("Saving roster (Line " + updateObj.rsl_line + " , " + dayOfTheWeek[updateObj.rsl_day.Value] + ") failed: " + XmlHttpRequest.responseJSON.error.message.value);
			}
		}
	});
}

function clearRosterDetail() {
	// Get all the roster details then delete them one by one
	// At least... do it this way to start with - maybe find another way at some later date
	//
	// Create a 'deferred' that can create a promise that can be passed back to the caller
	var d = $.Deferred();
	// Use an array to save the responses from the deletes
	var deletedRosterItems = [];

	getData("rsl_baserosterdetailSet", "rsl_baserosterdetailId", function(rosterDetails) {

		$.each(rosterDetails, function(i, v) {
			deletedRosterItems.push(deleteRosterDetailItem(v.rsl_baserosterdetailId));
		});

		// When all the updates (deletes) are finished respond with a resolve() or a reject()
		$.when.apply($, deletedRosterItems).done(function(result) {
			d.resolve(result);
		}).fail(function(error) {
			d.reject(error);
		});

	});

	return d.promise();		// return the promise()
}

function deleteRosterDetailItem(id) {
	var jsonEntity = window.JSON.stringify(id);

	return $.ajax({
		type: "POST",
		   contentType: "application/json; charaset=utf-8",
		   datatype: "json",
		   data: jsonEntity,
		   url: server + oData + "rsl_baserosterdetailSet(guid'" + id + "')",
		   beforeSend: function (XMLHttpRequest) {
			   //Specifiying this header ensures that the results will be returned as JSON
			   XMLHttpRequest.setRequestHeader("Accept", "application/json");
			   // Specify the HTTP method 
			   XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "DELETE");
		   }
	})
	.done(function (data, textStatus, XmlHttpRequest) {
		//alert("roster details deleted.");	//TODO make this a ui dialog
	})
	.fail(function (XmlHttpRequest, textStatus, errorThrown) {
		if (XmlHttpRequest && XmlHttpRequest.responseText ) {
			alert("Deleting roster item failed: " + XmlHttpRequest.responseJSON.error.message.value);
		}
	});
}

function get_CRM_Services() {
	return function() {
		var url;
		var d = $.Deferred();

		var query = "rsl_baserosterdetailId,rsl_line,rsl_day,";
		query = query + "rsl_baserosterdetail_rsl_dutydefinition/rsl_dutydefinitionId,";
		query = query + "rsl_baserosterdetail_rsl_dutydefinition/rsl_icon_url,";
		query = query + "rsl_baserosterdetail_rsl_dutydefinition/rsl_service";
		query = query + "&$expand=rsl_baserosterdetail_rsl_dutydefinition";

		$.when(
			getData("rsl_baserosterSet", "rsl_baserosterId", addBaseRosterId),

			//getData("ServiceSet", "Name,Description,ServiceId", addServices),
			getData("rsl_dutydefinitionSet", "rsl_service,rsl_dutydefinitionId,rsl_icon_url,rsl_starttime,rsl_endtime", addServices),
	
			//getData("rsl_baserosterdetailSet", "rsl_baserosterdetailId,rsl_line,rsl_service,rsl_day", addDetails)
			//rsl_baserosterdetailId,rsl_line,rsl_day,rsl_baserosterdetail_rsl_dutydefinition/rsl_service&$expand=rsl_baserosterdetail_rsl_dutydefinition
			//getData("rsl_baserosterdetailSet", "rsl_baserosterdetailId,rsl_line,rsl_day,rsl_baserosterdetail_rsl_dutydefinition/rsl_dutydefinitionId,rsl_baserosterdetail_rsl_dutydefinition/rsl_service&$expand=rsl_baserosterdetail_rsl_dutydefinition", addDetails)
			getData("rsl_baserosterdetailSet", query, addDetails)

		).done(function () { d.resolve();}).fail(function() {d.reject();});

		return d.promise();
	}
}

function getData(entity, select, success_callback) {

	url = server + oData + entity;
	url = url + "?$select=" + select;

	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		datatype: "json",
		url: url,
		crossDomain: true,
		beforeSend: function (XMLHttpRequest) {
			XMLHttpRequest.setRequestHeader("Accept", "application/json");
		},
		success: function (data, textStatus, XmlHttpRequest) { 
			if ( data.d.results ) {
				success_callback(data.d.results);
			} else {
				success_callback(data);
			}
		},
		error: function (XmlHttpRequest, textStatus, errorThrown) {
			//alert("OData 'Service' Select Failed: " + XmlHttpRequest.statusText);
			alert("OData call failed: " + XmlHttpRequest.responseJSON.error.message.value);
		}
	});
}

function get_Local_Services() {
	return function() {
	function sleep(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
				break;
			}
		}
	}
	//sleep(3000);
	addBaseRosterId([
			{
				"__metadata": {
					"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_baserosterSet(guid'2f75bc24-3b11-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_baseroster"
				}, "rsl_baserosterId": "2f75bc24-3b11-e611-ba00-005056860646"
			}
			]);


	// This replicates the action of getting the services from the server
	addServices([{
		"rsl_starttime": null,
		"rsl_endtime": null,
		"rsl_icon_url": "rsl_roster/images/late_shift.jpg",
		"rsl_dutydefinitionId":
		"f7a12762-5d3c-e611-ba00-005056860646",
		"rsl_service": {
			"Id": "88a920d8-36cf-e511-ba00-005056860646",
			"LogicalName": "service",
			"Name": "Station Block (3 RPOs)"
		}
	}, {
		"rsl_starttime": "06:00",
		"rsl_endtime": "14:00",
		"rsl_icon_url": "rsl_roster/images/early_shift.jpg",
		"rsl_dutydefinitionId": "6b70494d-6f3c-e611-ba00-005056860646",
		"rsl_service": {
			"Id": "a4da9679-1819-e611-ba00-005056860646",
			"LogicalName": "service",
			"Name": "Office Block (2 RPOs)"
	}
	}, {
		"rsl_starttime": "00:00",
		"rsl_endtime": "00:00",
		"rsl_icon_url": "rsl_roster/images/holiday.jpg",
		"rsl_dutydefinitionId": "d2961079-6f3c-e611-ba00-005056860646",
		"rsl_service": {
			"Id": "a10aca80-4f23-e611-ba00-005056860646",
			"LogicalName": "service",
			"Name": "Holiday"
		}
	}]);

	$('<div>').button({'label': "Save"}).click(saveRoster).insertAfter('#deleteLine');

	addDetails([
{
"__metadata": {
"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_baserosterdetailSet(guid'14011821-ad3e-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_baserosterdetail"
}, "rsl_baserosterdetailId": "14011821-ad3e-e611-ba00-005056860646", "rsl_line": 1, "rsl_day": {
"__metadata": {
"type": "Microsoft.Crm.Sdk.Data.Services.OptionSetValue"
}, "Value": 866810001
}, "rsl_baserosterdetail_rsl_dutydefinition": {
"__metadata": {
"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_dutydefinitionSet(guid'f7a12762-5d3c-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_dutydefinition"
}, "rsl_icon_url": "rsl_roster/images/late_shift.jpg", "rsl_dutydefinitionId": "f7a12762-5d3c-e611-ba00-005056860646", "rsl_service": {
"__metadata": {
"type": "Microsoft.Crm.Sdk.Data.Services.EntityReference"
}, "Id": "88a920d8-36cf-e511-ba00-005056860646", "LogicalName": "service", "Name": "Station Block (3 RPOs)"
}
}
}, {
"__metadata": {
"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_baserosterdetailSet(guid'7a9a1227-ad3e-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_baserosterdetail"
}, "rsl_baserosterdetailId": "7a9a1227-ad3e-e611-ba00-005056860646", "rsl_line": 2, "rsl_day": {
"__metadata": {
"type": "Microsoft.Crm.Sdk.Data.Services.OptionSetValue"
}, "Value": 866810002
}, "rsl_baserosterdetail_rsl_dutydefinition": {
"__metadata": {
"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_dutydefinitionSet(guid'6b70494d-6f3c-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_dutydefinition"
}, "rsl_icon_url": "rsl_roster/images/early_shift.jpg", "rsl_dutydefinitionId": "6b70494d-6f3c-e611-ba00-005056860646", "rsl_service": {
"__metadata": {
"type": "Microsoft.Crm.Sdk.Data.Services.EntityReference"
}, "Id": "a4da9679-1819-e611-ba00-005056860646", "LogicalName": "service", "Name": "Office Block (2 RPOs)"
}
}
}, {
"__metadata": {
"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_baserosterdetailSet(guid'86063533-ad3e-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_baserosterdetail"
}, "rsl_baserosterdetailId": "86063533-ad3e-e611-ba00-005056860646", "rsl_line": 3, "rsl_day": {
"__metadata": {
"type": "Microsoft.Crm.Sdk.Data.Services.OptionSetValue"
}, "Value": 866810003
}, "rsl_baserosterdetail_rsl_dutydefinition": {
"__metadata": {
"uri": "https://raspberry.mycrmhosted.net/XRMServices/2011/OrganizationData.svc/rsl_dutydefinitionSet(guid'd2961079-6f3c-e611-ba00-005056860646')", "type": "Microsoft.Crm.Sdk.Data.Services.rsl_dutydefinition"
}, "rsl_icon_url": "rsl_roster/images/holiday.jpg", "rsl_dutydefinitionId": "d2961079-6f3c-e611-ba00-005056860646", "rsl_service": {
"__metadata": {
"type": "Microsoft.Crm.Sdk.Data.Services.EntityReference"
}, "Id": "a10aca80-4f23-e611-ba00-005056860646", "LogicalName": "service", "Name": "Holiday"
}
}}
		]);

	}
}
