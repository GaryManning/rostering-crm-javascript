var baseRosterDetail = {
	//
	//	The Base Roster detail element
	//	Just makes it easy to process the individual cells in the table
	//	by having them all look after and process their own data
	//
	'baseRoster': "Raspberry Software Roster",	// TODO get the id rather than the name. base roster that this detail is linked to 
	'toString': function() {
		// Just displays the data in a nice format while debugging
		var updateObj = this.toUpdateObj();
		return "rsl_baseroster = {'LogicalName': '" + updateObj.rsl_baseroster.LogicalName
		   	+ ", 'Id': " + updateObj.rsl_baseroster.Id  + "}, rsl_line = " + updateObj.rsl_line
			+ ", rsl_service = {'LogicalName': " + updateObj.rsl_service.LogicalName + ", Id: " + updateObj.rsl_service.Id
			+ ", rsl_day = {'Value': " + updateObj.rsl_day.Value;
	},
	'toUpdateObj': function(line) {
		var updateObj = {};

		updateObj.rsl_baseroster = {'LogicalName': "rsl_baseroster", 'Id': this.rsl_baserosterId};
		//updateObj.rsl_line = this.line;
		updateObj.rsl_line = line;
		updateObj.rsl_day = {'Value': this.day};
		//updateObj.rsl_service = {'LogicalName': "service", 'Id': this.ServiceId};
		//updateObj.rsl_dutydefinitionId = this.DutyDefinitionId;
		//updateObj.rsl_baserosterdetailId = {'LogicalName': 'rsl_dutydefinition', 'Id': this.DutyDefinitionId};
		updateObj.rsl_dutydefinition = {'LogicalName': 'rsl_dutydefinition', 'Id': this.DutyDefinitionId};

		return updateObj;
	}
};

var dayOfTheWeek = {
	'866810000': "Sunday",
	'866810001': "Monday",
	'866810002': "Tuesday",
	'866810003': "Wednesday",
	'866810004': "Thursday",
	'866810005': "Friday",
	'866810006': "Saturday"
}

var ServiceNameRE = / \((\d+).*\)/; // Regexp to extract the service name from the service/rpo count

var rosterChangedFlag = false;

function init() {

	$('body').append('<div id="rsl-dialog-validation" title="Information"><p></p></div>');
	$('#rsl-dialog-validation').dialog({'autoOpen': false, 'modal': true, 'buttons': [{'text': "Ok", 'click': function() {$(this).dialog('close');}}]});

	$('#addLine').button({'label': "Add Line"}).click(function() { addLine(false, 0); addTableLinks();});
	$('#deleteLine').button({'label': "Delete Line"}).click(deleteLine);
	$('#saveRoster').button({'label': "Save Roster", 'disabled': true}).click(saveRoster);

	$('#rsl-table-base').on('click', ".rsl-row-number", function(event) {
		$(event.target).toggleClass('ui-state-highlight');
		$(event.target).nextAll().toggleClass('ui-state-highlight');
	});

	$('.rsl-unallocated-cell').droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		drop: function(event, ui) {
			ui.draggable.parent().parent().droppable('enable');

			var activity = ui.draggable.find('p').detach();
			ui.draggable.find('div').remove();
			ui.draggable.append(activity);
			ui.draggable.appendTo($(this).find('ul'));


			// Check to see if there are any of these services left
			var count = ui.draggable.data('count');

			checkUnallocated($(this), activity.text(), count);

			rosterChangedFlag = true;
			$('#saveRoster').button('option', 'disabled', false);
		},
	});

	$.when(addFirstLine()).then(getServices()).then(formatTable(true), addScrollbar());

}

function checkUnallocated(cell, activity, count) {
	var activities = cell.find('li').filter(function(i) {
		return $(this).text() === activity && !$(this).hasClass('ui-draggable-dragging');
	});

	if ( activities.length === count ) {
		activities.remove();
	}
}

function addServices(services) {
	var serviceList = $('#rsl-ul-service-list');

	$.each(services, function(i, v) {
		var a = [];
		var item = v.rsl_service.Name;

		if ( v.rsl_service.Name.match(ServiceNameRE) === null ) {
			v.count = 0;
		} else {
			v.count = parseInt(v.rsl_service.Name.match(ServiceNameRE)[1]);
		}

		v.activity = v.rsl_service.Name.replace(ServiceNameRE, "");

		//v.ServiceId = v.rsl_service.Id;
		v.DutyDefinitionId = v.rsl_dutydefinitionId;

		for (i=1; i < v.count; i++ ) {
			a.push(i);	// This makes a useful array for looping through later (rather than using a for loop)
		}

		v.a = a;

		serviceList.append(
			$('<li class="rsl-service-list-item">')
			.text(item)
			.attr('title', v.Description)
			.data(v)
			.draggable({
				'helper': "clone",
				'appendTo': "body",
				'start': function(event, ui) {
					$(ui['helper'][0]).addClass('ui-state-default');
					$(ui['helper'][0]).css('list-style-type', 'none');
					$(ui['helper'][0]).css('background', 'none');
					$(ui['helper'][0]).css('border', 'none');
				},
			})
			.tooltip()
			);
	});

}

function addScrollbar() {
var scrollPane = $( ".scroll-pane" ),
      //scrollContent = $( ".scroll-content" );
      scrollContent = $( "#rsl-ul-service-list" );
 
    //build slider
    var scrollbar = $( ".scroll-bar" ).slider({
	orientation: "vertical",
	value: 100,
      slide: function( event, ui ) {
        if ( scrollContent.height() > scrollPane.height() ) {
          scrollContent.css( "margin-top", Math.round(
            (100 - ui.value) / 100 * ( scrollPane.height() - scrollContent.height() - 20)
            //ui.value / 100 * ( scrollContent.height() - scrollPane.height() )
          ) + "px" );
        } else {
          scrollContent.css( "margin-top", 0 );
        }
      }
    });
 
    //append icon to handle
    var handleHelper = scrollbar.find( ".ui-slider-handle" )
    .mousedown(function() {
      scrollbar.width( handleHelper.width() );
    })
    .mouseup(function() {
      scrollbar.width( "100%" );
    })
    .append( "<span class='ui-icon ui-icon-grip-dotted-horizontal'></span>" )
    .wrap( "<div class='ui-handle-helper-parent'></div>" ).parent();
 
    //change overflow to hidden now that slider handles the scrolling
    scrollPane.css( "overflow", "hidden" );
 
    //size scrollbar and handle proportionally to scroll distance
    function sizeScrollbar() {
      var remainder = scrollContent.height() - scrollPane.height();
	  if ( remainder < 0 ) {
		  $('.scroll-bar-wrap').remove();
		  return;
	  }
      var proportion = remainder / scrollContent.height();
      var handleSize = scrollPane.height() - ( proportion * scrollPane.height() );
      scrollbar.find( ".ui-slider-handle" ).css({
        height: handleSize,
        "margin-top": -handleSize / 2
      });
      handleHelper.height( "" ).height( scrollbar.height() - handleSize );
    }
 
    //reset slider value based on scroll content position
    function resetValue() {
      var remainder = scrollPane.width() - scrollContent.width();
      var leftVal = scrollContent.css( "margin-left" ) === "auto" ? 0 :
        parseInt( scrollContent.css( "margin-left" ) );
      var percentage = Math.round( leftVal / remainder * 100 );
      scrollbar.slider( "value", percentage );
    }
 
    //if the slider is 100% and window gets larger, reveal content
    function reflowContent() {
        var showing = scrollContent.width() + parseInt( scrollContent.css( "margin-left" ), 10 );
        var gap = scrollPane.width() - showing;
        if ( gap > 0 ) {
          scrollContent.css( "margin-left", parseInt( scrollContent.css( "margin-left" ), 10 ) + gap );
        }
    }

	sizeScrollbar();
}

function addFirstLine() {
	//var d = $.Deferred();
	addLine(true, 1);
	//return d.promise();
}

function addTableLinks() {
	var cells = $('#rsl-table-base tbody tr td.rsl-cell-droppable');
	var antePrevCell, prevCell, nextCell;

	antePrevCell = $(cells.splice(0,1));
	prevCell = $(cells.splice(0,1));

	// Add the 'next' link to the second cell that would otherwise get missed
	antePrevCell.data('nextCell', prevCell);

	$.each(cells, function(i, v) {

		prevCell.data('prevCell', antePrevCell);
		prevCell.data('nextCell', $(v));

		antePrevCell = prevCell;
		prevCell = $(v);

	});

	// Add the 'prevous' link to the last cell (missed due to the way the links are made above)
	prevCell.data('prevCell', antePrevCell);

	// Link the very first row/column to the vary last row/column and vise versa
	$('#rsl-table-base tbody tr:nth(1) td:nth(1)').data('prevCell', $('#rsl-table-base tbody tr:last td:last'));
	$('#rsl-table-base tbody tr:last td:last').data('nextCell', $('#rsl-table-base tbody tr:nth(1) td:nth(1)'));
}

var checkValidDrop = function () {
	var ActivityWorkingMatrix = {
		'Station Block ': {
			'Station Block ': false,
			'Office Block ': true},
		'Office Block ': {
			'Office Block ': false,
			'Station Block ': true}
	}

	var ConsecutiveDays = 4;

	return function(cell, incoming) {

		var neighbour, valid, prevCount = 0, nextCount = 0;
		var activity = incoming.data('activity');

		// Check the 'previous' cell for compatatbility and also check back further for consecutive days worked
		neighbour = cell.data('prevCell').find('li');
		if ( neighbour.length === 1 ) {
			if ( !checkNeighbour(activity, neighbour.data('activity')) ) {
				return false;
			}
			prevCount = 1 + getPreviousCell(neighbour, 'prevCell');	// The initial '1' is the count for 'this' cell (which we already know has a shift in it)
		}

		// Same again for the 'next' cell
		neighbour = cell.data('nextCell').find('li');
		if ( neighbour.length === 1 ) {
			if ( !checkNeighbour(activity, neighbour.data('activity')) ) {
				return false;
			}
			nextCount = 1 + getPreviousCell(neighbour, 'nextCell');
		}

		// Check the count of consecutive days
		if ( (prevCount + nextCount + 1) >= ConsecutiveDays ) {
			$('#rsl-dialog-validation').find('p').text("You can't drop that item there owing to a run of consecutive working days");
			showInvalidMessage();
			return false;
		}

		/*
		 * Validation helper functions
		 */
		function getPreviousCell(cell, direction) {
			var neighbour = cell.parent().parent().data(direction).find('li');
			if ( neighbour.length === 1 ) {
				return 1 + getPreviousCell(neighbour, direction);
			}
			return 0;
		}

		function checkNeighbour(activity, neighbourActivity) {
			if ( ActivityWorkingMatrix[activity] === undefined ) {
				return true;
			}
			if ( ActivityWorkingMatrix[activity][neighbourActivity] !== true ) {
				$('#rsl-dialog-validation').find('p').text("You can't drop that item there owing to a conflict with a neighbouring working shift");
				showInvalidMessage();
				return false;
			}
			return true;
		}

		function showInvalidMessage() {
				cell.addClass('rsl-cell-invalid');
				neighbour.parent().parent().addClass('rsl-cell-invalid');
				$('#rsl-dialog-validation').dialog({'close': function() { cell.removeClass('rsl-cell-invalid'); neighbour.parent().parent().removeClass('rsl-cell-invalid');}});
				$('#rsl-dialog-validation').dialog('open');
		}

		return true;
	}
}();

function addLine(init, line) {
	var firstRow = $('#rsl-table-firstrow');
	var rows = parseInt(firstRow.attr('rowspan'));
	var newLine;
	var newCell, firstCell, lastCell;

	if ( line === 0 ) {
		// This has been instigated by the button press so we need to work out what the line number should be
		line = rows;
	}

	if ( rows === 1 ) {		// ie the first row (it is included in the index.html but is expendable)
		// Remove the current row
		firstRow.siblings().remove();
	}

	// Create the new line (contains the line number)
	newLine = $('<tr>').append($('<td class="rsl-centre-text rsl-row-number">').text(line));

	if ( rows < line ) {
		// If this row is larger than the current line count then go back and insert all the intervening lines
		addLine(false, line - 1);
	}

	// Update the row span of the first row
	//$('#rsl-table-firstrow').attr('rowspan', rows + 1);
	$('#rsl-table-firstrow').attr('rowspan', line + 1);

	// Add each of the cells in the row
	$.each([0,1,2,3,4,5,6], function(i, v) {

		newCell = $('<td class="rsl-cell-droppable">').droppable({
				activeClass: "ui-state-default",
				hoverClass: "ui-state-hover",
				drop: function( event, ui ) {
					//var ServiceId = ui.draggable.data('ServiceId');
					//var ServiceId = ui.draggable.data('ServiceId');
					var DutyDefinitionId = ui.draggable.data('DutyDefinitionId');
					var activity = ui.draggable.data('activity');
					var imageUrl = ui.draggable.data('rsl_icon_url');
					var count = ui.draggable.data('count');
					var a = ui.draggable.data('a');
					var neighbour;

					// TODO - add some validation here: is it ok for the service to be dropped here?
					// (for instance, has there been more than 9 hours since the previous shift finished
					// or no more than 11 days without a break?)
					if ( !checkValidDrop($(this), ui.draggable) ) {
						return;
					}

					// Create a new 'li' to add to the cell
					var detailObj = Object.create(baseRosterDetail);
					detailObj.line = $($(this).parent().children()[0]).text();
					detailObj.activity = ui.draggable.data('activity');
					//detailObj.ServiceId = ui.draggable.data('ServiceId');
					detailObj.DutyDefinitionId = ui.draggable.data('DutyDefinitionId');
					detailObj.day = 866810000 + i;	// 866810000 = Sunday, 866810006 = Saturday

					//var li = $('<li>').text(activity).data({'activity': activity, 'count': count, 'a': []})
					var li = $('<li>').data({'activity': activity, 'rsl_icon_url': imageUrl, 'count': count, 'a': []})
						.data('detail', detailObj)
						.draggable({
							'revert': "invalid",
							'axis': "y",
							'helper': "clone",
							'cursorAt': {'left': 30, 'top': 10},
							'start': function(event, ui) {
								var activity = $(ui.helper[0]).find('p').detach();
								$(ui.helper[0]).find('div').remove();
								$(ui.helper[0]).append(activity);
								$(ui.helper[0]).appendTo($(this).find('ul'));
							}
						}).appendTo($(this).find('ul'));
					$(this).find('ul').append(li);

					var img = $('<img>').attr('src', imageUrl).css({'width': $(this).width() + 1 + "px", 'height': "10px"});
					var div = $('<div>').append(img).append($('<p class="rsl-centre-text">').text(activity));
					li.append(div);

					if ( ui.draggable.hasClass('rsl_base-roster-original') ) {
						li.addClass('rsl_base-roster-original');
					}

					// This should only get executed if it has been dropped from the 'service list'
					// on the left of the window and adds the extra services to the 'unallocated' section
					// (relies on the 'a' array having some length other than 0)
					$.each(a, function(j, w) {
						$('<li>').text(activity).data({'activity': activity, 'rsl_icon_url': imageUrl, 'DutyDefinitionId': DutyDefinitionId, 'count': count, 'a': []})
						.draggable({
							'revert': "invalid",
							'axis': "y",
							'helper': 'clone',
							'cursorAt': {'left': 30, 'top': 10},
						}).appendTo('#rsl-unallocated-'+i);
					});

					if ( !ui.draggable.parent().parent().hasClass('scroll-content') ) {
						// Stop anything else being dropped in the cell
						ui.draggable.parent().parent().droppable('enable');

						if ( a.length === 0 ) {
							ui.draggable.remove();
						}
					}
					$(this).droppable('disable');

					rosterChangedFlag = true;
					$('#saveRoster').button('option', 'disabled', false);

				}
		});

		newCell.append($('<ul>'));

		newLine.append(newCell);
		
	});

	// Add the line to the page
	$('#rsl-table-base tbody').append(newLine);

	if ( rows === 1 ) {
		// If this is the first row then format the table
		formatTable(init);
	}

	// Update the header and footer number (blank) columns so that the table is still nicely formatted
	var numberColumn = $($('#rsl-table-base .rsl-row-number')[0]).width();
	$($('#rsl-table-base thead tr:nth-child(2)').children()[1]).width(numberColumn);
	$($('#rsl-table-base tfoot tr:nth-child(2)').children()[1]).width(numberColumn);

	return;
}
/*
		$('<li class="rsl_base-roster-original">').text(detailObj.activity).data({'activity': detailObj.activity, 'ServiceId': detailObj.ServiceId, 'count': 1, 'a': []})
			.data('detail', detailObj)
			.draggable({
				'revert': "invalid",
				'axis': "y",
				'helper': "clone",
			}).appendTo($(column).find('ul'));
 */
function createDraggableActivity(detailObj) {
	var li = $('<li>').data({'activity': detailObj.activity, 'DutyDefinitionId': detailObj.DutyDefinitionId, 'rsl_icon_url': detailObj.imageUrl, 'count': detailObj.count, 'a': []})
		.data('detail', detailObj)
		.draggable({
			'revert': "invalid",
		'axis': "y",
		'helper': "clone",
		'start': function(event, ui) {
			var activity = $(ui.helper[0]).find('p').detach();
			$(ui.helper[0]).find('div').remove();
			$(ui.helper[0]).append(activity);
			$(ui.helper[0]).appendTo($(this).find('ul'));
		}
		});
	return li;
}

function deleteLine() {
	var firstRow = $('#rsl-table-firstrow');
	var rows = parseInt(firstRow.attr('rowspan'));
	var resizeColumns = false;

	// Get the highlighted rows
	var deletes = $('#rsl-table-base .rsl-row-number.ui-state-highlight').parent();
	if ( deletes.length > 0 ) {
		firstRow.attr('rowspan', rows - deletes.length);
		$.each(deletes, function(i, v) {
			var cells = $(v).children().not('.rsl-row-number');
			$.each(cells, function(j, w) {
				// Find cells that have 'services' in them...
				$.each($(w).find('li'), function(k, x) {
					// and move them to the 'unallocated' row
					$(x).detach().appendTo('#rsl-unallocated-'+j);
					
					// Check to see if they are all in 
					checkUnallocated($('#rsl-unallocated-'+j), $(x).data('service'), $(x).data('count'));
				});
			});
			if ( $(v).text() == 1 ) {
				resizeColumns = true;
			}
		});
		deletes.remove();
		if ( resizeColumns ) {
			//formatTable(false);
		}

		// Renumber the rows
		$('#rsl-table-base .rsl-row-number').each(function(i, v) {
			$(v).text(i + 1);
		});

		rosterChangedFlag = true;
		$('#saveRoster').button('option', 'disabled', false);
	}
}

function formatTable(init) {
	//
	// Get all column widths to match up across the sections
	//

	var table = $('#rsl-table-base');

	// Sort out the height of the tbody section
	// table height - thead height - tfoot height
	//var tbodyHeight = table.height() - table.find('thead').height() - table.find('tfoot').height();
	var tbodyHeight = window.innerHeight - table.find('thead').height() - table.find('tfoot').height() - 12;	// 12px fudge factor
	table.find('tbody').height(tbodyHeight);

	// The service list should be the widest column so get its width
	//var col1Width = table.find('tbody tr:nth-child(1) td').width();
	//var col1Width = $('#rsl-ul-service-list').width();
	var col1Width = $('.scroll-content').width();

	if ( init ) {
		col1Width = col1Width + 20; // for the scrollbar - only if this the initial call
		col1Width += parseInt($('.scroll-content').css('paddingLeft'));
		col1Width += parseInt($('.scroll-content').css('paddingRight'));
	}
	$('#rsl-ul-service-list').width(col1Width);

	// And the width of the 'number' column
	var col2Width = table.find('tbody tr:nth-child(2) td:nth-child(1)').width();

	// Set the width of the (empty) left-hand column (it will be under the service list)
	table.find('tbody tr:nth-child(1) td').width(col1Width);

	var colRest = (table.width() - col1Width - col2Width) / 7;

	// Get the column widths of the second row (the one with the days)
	var colWidth = table.find('thead tr:nth-child(2)').children().map(function() {
		//return $(this).width() + 1;
		//return col1Width;
		return colRest - 30;
	}).get();

	// Remove the first column's width and replace it with the 'Service' coulmn's width
	colWidth.splice(0, 1, col1Width);

	// Remove the second column's width and replace it with the 'number' coulmn's width
	colWidth.splice(1, 1, col2Width);

	// Set the width of the columns in the second row of the tfoot section
	table.find('tfoot tr:nth-child(2)').children().each(function(i, v) {
		$(v).width(colWidth[i]);
	});    

	// And the thead section - though really only the second column needs changing
	table.find('thead tr:nth-child(2)').children().each(function(i, v) {
		$(v).width(colWidth[i]);
	});    

	// Remove the first coulmn width (not needed for the tbody rows as the first column spans them all)
	colWidth.shift();

	// Set the width of the columns in the second row of the tbody section
	// The first column (in reality the second in the table) needs to be narrower
	// than the rest so only add an increment to the rest
	var rest = 0;
	table.find('tbody tr:nth-child(2)').children().each(function(i, v) {
		$(v).width(colWidth[i] + rest);
		rest = 20;
	});    

	// Set a flag to say this the "row with all the sizes", if it is deleted then the widths will need recalculating
	table.find('tbody tr:nth-child(2)').data('columnWidths', true);

	// Get the position of the 'services' cell on the page and its padding to enable the 'services' div to be placed on top of it
	var services = $('#rsl-table-firstrow');
	var pos = services.position();
	var paddingTop = parseFloat(services.css('paddingTop'));
	var paddingBottom = parseFloat(services.css('paddingBottom'));

	// Also, get the 'tbody's height and max-height so that it can be used to match the 'services' div to the 'services' cell
	var height = table.find('tbody').height();
	var maxHeight = parseFloat(table.find('tbody').css('maxHeight'));
	//$('#rsl-div-service-list').css({'top': pos.top, 'left': pos.left, 'height': height - paddingTop - paddingBottom, 'maxHeight': maxHeight - paddingTop -paddingBottom});
	$('#rsl-div-service-list').css({'top': pos.top - 1, 'left': pos.left - 1, 'height': height, 'maxHeight': maxHeight});
}

function saveRosterFromForm(context) {
	// Check to see if the roster has been changed and needs saving
	var contextWindow = Xrm.Page.getControl('WebResource_roster_baseConfig').getObject().contentWindow.window;
	if ( !contextWindow.$('#saveRoster').button('option', 'disabled') ) {
		contextWindow.$('#rsl-dialog-validation').find('p').text("The amended roster must be saved first.");
		contextWindow.$('#rsl-dialog-validation').dialog('open');
		context.getEventArgs().preventDefault();
		return;
	}

	$.when(contextWindow.saveRoster(context)).done(function() {
		window.location.reload(true);
	});
}

function saveRoster(context) {
	//
	//	Save the roster to the database
	//	(TODO might need the original config deleting first so that changes overwrite those in the CRM system
	//
	
	if ( !rosterChangedFlag ) {
		return;
	}

	// Clear the exising roster detail records from the current roster
	var d = $.Deferred();

	$.when(clearRosterDetail()).done(function(){

		//console.log("Writing new roster to the database");

		var createdRosterDetails = [];
		var tableBody = $('#rsl-table-base tbody');
		var rows = tableBody.find('tr');
		rows.splice(0, 1);
		var td, cell;

		$.each(rows, function(i, v) {
			td = $(v).find('td');
			td.splice(0,1);
			$.each(td, function(j, w) {
				if ( $(w).find('ul li').data('activity') !== undefined ) {
					cell = $(w).find('ul li').data('detail');
					createdRosterDetails.push(createRosterDetail(cell.toUpdateObj(i+1)));
				}
			});
		});

		//$.when.apply($, createdRosterDetails).done(function() { alert("New roster created"); d.resolve();}).fail(function() { d.reject();});
		$.when.apply($, createdRosterDetails).done(function() {
			//alert("New roster created");
			var buttons = $('#rsl-dialog-validation').dialog('option', 'buttons');
			$('#rsl-dialog-validation').dialog('option', 'buttons', [{
				'text': 'Ok', 
				'click': function() {
					$('#rsl-dialog-validation').dialog('option', 'buttons', buttons);
					$(this).dialog('close');
					rosterChangedFlag = false;
					$('#saveRoster').button('option', 'disabled', true);
					d.resolve();
				}}]);
			$('#rsl-dialog-validation').find('p').text("New roster created");
			$('#rsl-dialog-validation').dialog('open');
			//d.resolve();
		}).fail(function() {
			d.reject();
		});

	}).fail(function(data) {
		if ( context ) {
			alert("Clearing previous roster entries failed");
			context.getEventArgs().preventDefault();
		}
	});

	return d.promise();
}

function addDetails(details) {
	var table = $('#rsl-table-base tbody');
	var row, column;
	var detailObj;

	$.each(details, function(i, v) {
		detailObj = Object.create(baseRosterDetail);
		//detailObj.baseRoster = v.rsl_baserosterdetailId;
		detailObj.line = v.rsl_line;
		detailObj.activity = v.rsl_baserosterdetail_rsl_dutydefinition.rsl_service.Name.replace(ServiceNameRE, "");
		detailObj.rsl_icon_url = v.rsl_baserosterdetail_rsl_dutydefinition.rsl_icon_url;
		//detailObj.ServiceId = v.rsl_service.Id;
		detailObj.DutyDefinitionId = v.rsl_baserosterdetail_rsl_dutydefinition.rsl_dutydefinitionId; 
		detailObj.day = v.rsl_day.Value;

		// Add the 'detail' to the table (row:'line', col:'day')
		row = table.find('tr')[v.rsl_line];
		if ( row === undefined ) {
			addLine(false, v.rsl_line);
			row = table.find('tr')[v.rsl_line];
		}

		column = $(row).find('td')[v.rsl_day.Value - 866809999]; //(866810000 - 1 so that Sunday is '1' - column 1, not column 0)
		//var li = $('<li class="rsl_base-roster-original">').text(detailObj.activity).data({'activity': detailObj.activity, 'DutyDefinitionId': detailObj.DutyDefinitionId, 'count': 1, 'a': []})
		var li = $('<li>').data({'activity': detailObj.activity, 'DutyDefinitionId': detailObj.DutyDefinitionId, 'rsl_icon_url': detailObj.rsl_icon_url, 'count': 1, 'a': []})
			.data('detail', detailObj)
			.draggable({
				'revert': "invalid",
				'axis': "y",
				'helper': "clone",
			}).appendTo($(column).find('ul'));

		var img = $('<img>').attr('src', detailObj.rsl_icon_url).css({'width': $(column).width() + 1 + "px", 'height': "10px"});
		var div = $('<div>').append(img).append($('<p class="rsl-centre-text">').text(detailObj.activity));
		li.append(div);

		$(column).droppable('disable');
	});

	addTableLinks();
}

function addBaseRosterId(id) {
	baseRosterDetail.rsl_baserosterId = id[0].rsl_baserosterId;
}

if ( typeof Object.create !== 'function') {
	Object.create = function(o) {
		var F = function () {};
		F.prototype = o;
		return new F();
	};
}
