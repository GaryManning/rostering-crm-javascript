var userName;

function init() {

	var Xrm = window.parent.Xrm;

	var UserId = Xrm.Page.context.getUserId();

	getData("SystemUserSet(guid'" + UserId + "')", "FullName", function(data) { userName = data.d['FullName'];});

	Xrm.Page.data.entity.addOnSave(function (context) {
		updateAllComments();
	});

	Xrm.Page.getAttribute('rsl_comment').setSubmitMode("never");

	formatComments();

}

function formatComments() {

	var li, who, comment, when, date;

	var Xrm = window.parent.Xrm;

	var json = JSON.parse(Xrm.Page.getAttribute('rsl_allcomments').getValue());

	if ( json.comments.length === 0 ) {

		li = $('<li>').append();

		$('#rsl_commentList').append($('<span>').text("No comments"));

	}

	$.each(json.comments, function(i, v) {

		if ( v.comment === null ) {
			v.comment = "(saved with no comment)";
		}
		
		date = new Date(v.when.replace(/(\d\d)\/(\d\d)\/(.*)/, "$2/$1/$3")).toString().slice(0,21);
		who = $('<div class="rsl_swapComment-name">').text(v.who);
		when = $('<div class="rsl_swapComment-date">').text(date);
		comment = $('<div class="rsl_swapComment-container">').append($('<span>').text(v.comment));


		li = $('<li>').append(who).append(when).append(comment);

		$('#rsl_commentList').append(li);
	});
}

function formatDate() {
	var date = new Date();
	return ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear() + " " + ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2) + ":" + ('0' + date.getSeconds()).slice(-2);
}

function updateAllComments() {

	var who, when;

	var Xrm = window.parent.Xrm;

	var json = JSON.parse(Xrm.Page.getAttribute('rsl_allcomments').getValue());

	//who = Xrm.getUser();
	who = userName;
	//when = new Date().toString().slice(0,21); // May need refining - we'll see how it goes
	when = formatDate();
	comment = Xrm.Page.getAttribute('rsl_comment').getValue();

	if ( comment === null ) {
		// No comment entered, so just return
		// Note: there is an issue with saving the form with no comments
		//		It can be reproduced by trying to save the form initially with no comments
		//		but then entering text in the comments field and trying to save again but the field
		//		is still found to be null so the save just comes through here and exits with saving.
		//		This is an issue with the field still having focus and CRM still sees it as an empty
		//		field. 'Saving and Exiting' works, as does clicking away from the field (to remove focus) and saving.
		//		We decided to leave things as they are for the moment and see if any issues crop up in the future.
		return;
	}

	json.comments.unshift({'who': who, 'comment': comment, 'when': when});

	Xrm.Page.getAttribute('rsl_allcomments').setValue(JSON.stringify(json));
}
